# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from PyQt4.QtCore import QSettings, QObject, pyqtSignal
from singleton import Singleton


class Settings(QObject, Singleton):
	# signals #
	systray_enabled_changed = pyqtSignal(bool)
	fgfs_messages_enabled_changed = pyqtSignal(bool)
	fgfs_hostname_changed = pyqtSignal(str)
	fgfs_in_port_changed = pyqtSignal(int)
	fgfs_out_port_changed = pyqtSignal(int)
	mumble_command_changed = pyqtSignal(str)
	
	def __init__(self, parent = None):
		QObject.__init__(self, parent)
		Singleton.__init__(self)
		self._settings = QSettings("JBurbach", "GrumbleClient", self)

	def get_systray_enabled(self):
		return self._settings.value("systray_enabled", True).toBool()

	def set_systray_enabled(self, enabled):
		if self.get_systray_enabled() != enabled:
			self._settings.setValue("systray_enabled", enabled)
			self.systray_enabled_changed.emit(enabled)			

	def get_fgfs_messages_enabled(self):
		return self._settings.value("fgfs_messages_enabled", True).toBool()

	def set_fgfs_messages_enabled(self, enabled):
		if self.get_fgfs_messages_enabled() != enabled:
			self._settings.setValue("fgfs_messages_enabled", enabled)
			self.fgfs_messages_enabled_changed.emit(enabled)

	def get_fgfs_hostname(self):
		return self._settings.value("fgfs_hostname", "localhost").toString()

	def set_fgfs_hostname(self, hostname):
		if self.get_fgfs_hostname() != hostname:
			self._settings.setValue("fgfs_hostname", hostname)
			self.fgfs_hostname_changed.emit(hostname)

	def get_fgfs_out_port(self):
		return self._settings.value("fgfs_out_port", 6556).toUInt()

	def set_fgfs_out_port(self, port):
		if self.get_fgfs_out_port() != port:
			self._settings.setValue("fgfs_out_port", port)
			self.fgfs_out_port_changed.emit(port)

	def get_fgfs_in_port(self):
		return self._settings.value("fgfs_in_port", 6557).toUint()

	def set_fgfs_in_port(self, port):
		if self.get_fgfs_in_port() != port:
			self._settings.setValue("fgfs_in_port", port)
			self.fgfs_out_port_changed.emit(port)

	def get_mumble_command(self):
		return self._settings.value("mumble_command", "").toString()

	def set_mumble_command(self, command):
		if self.get_mumble_command() != command:
			self._settings.setValue("mumble_command", command)
			self.mumble_command_changed.emit(command)
