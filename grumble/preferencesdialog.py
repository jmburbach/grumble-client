# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt


class PreferencesDialog(QtGui.QDialog):

	def __init__(self, parent = None):
		QtGui.QDialog.__init__(self, parent)
		self.setWindowTitle("Preferences - Grumble Client")
		
		self.enable_fgfs_messages_checkbox = QtGui.QCheckBox(self)
		self.enable_fgfs_messages_checkbox.setText(self.tr("Display messages in FlightGear?"))
		self.enable_fgfs_messages_checkbox.setToolTip(self.tr("Whether or not Grumble should display messages inside FlightGear"))

		self.enable_systray_checkbox = QtGui.QCheckBox(self)
		self.enable_systray_checkbox.setText(self.tr("Run in the system tray?"))
		self.enable_systray_checkbox.setToolTip(self.tr("Whether or not Grumble should run in the system tray."))

		self.mumble_path_edit = QtGui.QLineEdit(self)
		self.mumble_path_edit.setToolTip(self.tr("Mumble command or full path to the mumble executable"))

		self.mumble_path_button = QtGui.QPushButton(self)
		self.mumble_path_button.setText("...")
		self.mumble_path_button.setToolTip(self.tr("Open file browser to locate file"))

		self.fgfs_host_edit = QtGui.QLineEdit(self)
		self.fgfs_host_edit.setToolTip(self.tr("Host name or IP address of the computer running FlightGear"))

		self.fgfs_out_port_edit = QtGui.QLineEdit(self)
		self.fgfs_out_port_edit.setToolTip(self.tr("Port number that FlightGear is configured to send data on"))
		self.fgfs_out_port_edit.setValidator(QtGui.QIntValidator(0, 65535, self))
		
		self.fgfs_in_port_edit = QtGui.QLineEdit(self)
		self.fgfs_in_port_edit.setToolTip(self.tr("Port number that FlightGear is configured to receive data on"))
		self.fgfs_in_port_edit.setValidator(QtGui.QIntValidator(0, 65535, self))
				
		# Grumble group

		grumble_box = QtGui.QGroupBox(self)
		grumble_box.setTitle("Grumble")

		grumble_layout = QtGui.QVBoxLayout()
		grumble_layout.addWidget(self.enable_systray_checkbox)
		grumble_layout.addWidget(self.enable_fgfs_messages_checkbox)
		grumble_box.setLayout(grumble_layout)
		
		# Mumble group

		mumble_box = QtGui.QGroupBox(self)
		mumble_box.setTitle("Mumble")

		mumble_path_label = QtGui.QLabel(self)
		mumble_path_label.setText(self.tr("Path"))
		
		mumble_path_layout = QtGui.QHBoxLayout()
		mumble_path_layout.addWidget(mumble_path_label)
		mumble_path_layout.addWidget(self.mumble_path_edit)
		mumble_path_layout.addWidget(self.mumble_path_button)

		mumble_box.setLayout(mumble_path_layout)

		# FlightGear group

		fgfs_box = QtGui.QGroupBox(self)
		fgfs_box.setTitle("FlightGear")

		fgfs_host_label = QtGui.QLabel(self)
		fgfs_host_label.setText(self.tr("Host"))

		fgfs_out_port_label = QtGui.QLabel(self)
		fgfs_out_port_label.setText(self.tr("Out Port"))

		fgfs_in_port_label = QtGui.QLabel(self)
		fgfs_in_port_label.setText(self.tr("In Port"))

		top_layout = QtGui.QHBoxLayout()
		top_layout.addWidget(fgfs_host_label)
		top_layout.addWidget(self.fgfs_host_edit)

		bottom_layout = QtGui.QHBoxLayout()
		bottom_layout.addWidget(fgfs_out_port_label)
		bottom_layout.addWidget(self.fgfs_out_port_edit)
		bottom_layout.addWidget(fgfs_in_port_label)
		bottom_layout.addWidget(self.fgfs_in_port_edit)

		fgfs_layout = QtGui.QVBoxLayout()
		fgfs_layout.addLayout(top_layout)
		fgfs_layout.addLayout(bottom_layout)
		fgfs_box.setLayout(fgfs_layout)

		# Button stuff

		line = QtGui.QFrame(self)
		line.setFrameShape(QtGui.QFrame.HLine)
		line.setFrameShadow(QtGui.QFrame.Sunken)
		
		button_box = QtGui.QDialogButtonBox(self)
		button_box.setOrientation(Qt.Horizontal)
		button_box.setStandardButtons(QtGui.QDialogButtonBox.Close)
		
		main_layout = QtGui.QVBoxLayout()
		main_layout.setSpacing(5)
		main_layout.setMargin(5)

		main_layout.addWidget(grumble_box)
		main_layout.addWidget(mumble_box)
		main_layout.addWidget(fgfs_box)
		main_layout.addWidget(line)
		main_layout.addWidget(button_box)
		self.setLayout(main_layout)

		self.adjustSize()
