# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import weakref, gc

class Singleton(object):
	__instance = None

	def __init__(self):
		assert self.__class__.__instance is None
		self.__class__.__instance = self
	
	@classmethod
	def get_instance(_class):
		if _class.__instance is None:
			return None
		return weakref.proxy(_class.__instance)

	@classmethod
	def del_instance(_class):
		del _class.__instance
		_class.__instance = None
		gc.collect()
