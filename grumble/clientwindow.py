from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt
import sys


class ClientWindow(QtGui.QMainWindow):

	def __init__(self):
		QtGui.QMainWindow.__init__(self)
		self.setWindowTitle("Grumble Client")
		self.resize(600, 300)

		self.create_actions()
		self.create_menus()

		central = QtGui.QWidget(self)
		
		self.atc_list_widget = QtGui.QListWidget(central)
		self.atc_list_widget.addItem("KBWI_TWR    TWR    123.150    mumble://fooserver.com/KBWI_TWR/?version=1.2.2")

		layout = QtGui.QVBoxLayout()
		layout.addWidget(self.atc_list_widget)
		central.setLayout(layout)

		self.setCentralWidget(central)

	def create_actions(self):
		self.quit_action = QtGui.QAction(self.tr("&Quit"), self)
		self.quit_action.triggered.connect(self.handle_quit)

		self.configure_action = QtGui.QAction(self.tr("&Configure Grumble Client"), self)
		self.configure_action.triggered.connect(self.handle_configure)

		self.about_action = QtGui.QAction(self.tr("&About Grumble Client"), self)
		self.help_action = QtGui.QAction(self.tr("&Grumble Client Help"), self)

		self.connect_action = QtGui.QAction(self.tr("&Connect"), self)
		self.disconnect_action = QtGui.QAction(self.tr("&Disconnect"), self)
		self.provide_atc_action = QtGui.QAction(self.tr("&Provide ATC"), self)

		if sys.platform == "darwin":
			self.quit_action.setMenuRole(QtGui.QAction.QuitRole)
			self.configure_action.setMenuRole(QtGui.QAction.PreferencesRole)
			self.about_action.setMenuRole(QtGui.QAction.AboutRole)

	def create_menus(self):
		if sys.platform == "darwin":
			self.menubar = QtGui.QMenuBar()
			dummy = self.menubar.addMenu("dummy")
			dummy.addAction(self.quit_action)
			dummy.addAction(self.configure_action)
			dummy.addAction(self.about_action)
		else:
			self.menubar = self.menuBar()
			self.grumble_menu = self.menubar.addMenu("Grumble Client")
			self.grumble_menu.addSeparator()
			self.grumble_menu.addAction(self.quit_action)

		self.network_menu = self.menubar.addMenu(self.tr("Network"))
		self.network_menu.addAction(self.connect_action)
		self.network_menu.addAction(self.disconnect_action)
		self.network_menu.addSeparator()
		self.network_menu.addAction(self.provide_atc_action)

		self.help_menu = self.menubar.addMenu(self.tr("&Help"))
		self.help_menu.addAction(self.help_action)

	def handle_configure(self):
		pass

	def handle_quit(self):
		QtGui.QApplication.exit()
