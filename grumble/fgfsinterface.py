import socket, select
from singleton import Singleton


class FGFSInterface(Singleton):

	def __init__(self, host = "localhost", out_port = 16556, in_port = 16557):
		Singleton.__init__(self)
		self.host = host
		self.out_port = out_port
		self.in_port = in_port

		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.socket.bind((host, out_port))
		self.socket.setblocking(False)

		self.props = {}
		self.msgbuf = []

	def process_network(self):
		nbytes_in = nbytes_out = 0
		while select.select([self.socket], [], [], 0)[0]:
			data, addr = self.socket.recvfrom(1024)
			nbytes_in += len(data)
			values = data.strip().split(';')
			self.props["frequency"] = values[0]
			self.props["latitude"] = values[1]
			self.props["longitude"] = values[2]
			self.props["callsign"] = values[3]

		while select.select([], [self.socket], [], 0)[1] and self.msgbuf:
			msg = self.msgbuf[0]
			nbytes_out += len(msg)
			self.msgbuf = self.msgbuf[1:]
			self.socket.sendto(msg, (self.host, self.in_port))

		return (nbytes_in, nbytes_out)

	@property
	def latitude(self):		
		lat = self.props.get("latitude")
		return lat and float(lat) or None

	@property
	def longitude(self):
		lon = self.props.get("longitude")
		return lon and float(lon) or None

	@property
	def callsign(self):
		return self.props.get("callsign")

	@property
	def frequency(self):
		return self.props.get("frequency")

	def post_message(self, format, *args):
		self.msgbuf.append("grumble: %s\n" % (format % args))


if __name__ == "__main__":
	import code
	FGFSInterface()
	code.interact(local = locals())
