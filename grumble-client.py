import sys
from PyQt4.QtGui import QApplication
from grumble.clientwindow import ClientWindow
from grumble.settings import Settings


if __name__ == "__main__":
	app = QApplication(sys.argv)
	win = ClientWindow()
	win.show()
	sys.exit(app.exec_())
